package hussein.com.utils

import hussein.com.domain.base.Unique
import kotlinx.serialization.*
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.encoding.encodeStructure


const val BASE_URL = "https://backend-compose-playground-my-portfolio.onrender.com"
const val BASE_STATIC_FILES_URL = "static"
const val BASE_ARTICLES_IMAGES_URL = "static/articles"
const val PLATFORMS_IMAGES_URL = "$BASE_URL/$BASE_STATIC_FILES_URL/platforms"
const val LINKEDIN_POSTS_PATH = "static/linkedinPosts/linkedinPosts.json"

@Serializable(with = PlatformSerializer::class)
enum class Platforms(val generalName: String, val logoPath: String) : Unique {
    LINKED_IN("Linked In", "$PLATFORMS_IMAGES_URL/linkedIn.png"),
    MEDIUM("Medium", "$PLATFORMS_IMAGES_URL/medium.png");

    override val uniqueId: Int
        get() = generalName.hashCode()

    override fun toString(): String {
        return super.toString()
    }

}

@OptIn(ExperimentalSerializationApi::class)
@Serializer(forClass = Platforms::class)
object PlatformSerializer {

    override fun serialize(encoder: Encoder, value: Platforms) {
        encoder.encodeStructure(
            descriptor
        ) {
            encodeStringElement(descriptor, 0, value.generalName)
            encodeStringElement(descriptor, 1, value.logoPath)
        }

    }
}