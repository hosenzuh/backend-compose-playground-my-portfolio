package hussein.com.utils

import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.server.application.*
import io.ktor.util.pipeline.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.core.parameter.ParametersDefinition
import org.koin.core.qualifier.Qualifier
import org.koin.ktor.ext.inject
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.util.function.Predicate


suspend fun PartData.FileItem.saveFilePartToServer(
    prefixPath: String,
): Boolean {
    val fileName = originalFileName
    val createdFile = File("${prefixPath}/${fileName}").apply {
        try {
            Files.createDirectories(Path.of(prefixPath))
            createNewFile()
        } catch (exception: Exception) {
            println(exception)
            return false
        }
    }
    return withContext(Dispatchers.IO) {
        try {
            streamProvider().use { inputStream ->
                createdFile.outputStream().buffered().use {
                    inputStream.copyTo(it)
                }
            }
            return@withContext true
        } catch (e: Exception) {
            return@withContext false
        }
    }
}

inline fun <reified T : Any> PipelineContext<Unit, ApplicationCall>.inject(
    qualifier: Qualifier? = null,
    noinline parameters: ParametersDefinition? = null
) = this.context.inject<T>(qualifier, parameters)

fun List<PartData>.getFormItemValueByName(name: String) =
    first { it.name == name }.let { it as PartData.FormItem }.value

fun List<PartData>.getFormItemValueByNameOrNull(name: String) =
    firstOrNull { it.name == name }?.let { it as? PartData.FormItem }?.value

fun List<PartData>.getFileItemByName(name: String) =
    first { it.name == name }.let { it as PartData.FileItem }

fun List<PartData>.getFileItemByNameOrNull(name: String) =
    firstOrNull { it.name == name }?.let { it as? PartData.FileItem }

fun List<PartData>.getFileItemsByContentSuperType(contentType: ContentType) =
    filter { it.contentType?.contentType == contentType.contentType }.map { it as PartData.FileItem }

fun List<PartData>.getFileItemsByPredicate(predicate: (PartData) -> Boolean) =
    filter(predicate).map { it as PartData.FileItem }
