package hussein.com.utils

import hussein.com.domain.base.Unique
import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction
import kotlin.reflect.KProperty

suspend fun <T> dbQuery(block: suspend () -> T): T =
    newSuspendedTransaction(Dispatchers.IO) {
        addLogger(StdOutSqlLogger)
        block()
    }

fun <T> loggedTransaction(db: Database? = null, statement: Transaction.() -> T) = transaction(db) {
    addLogger(StdOutSqlLogger)
    statement()
}


class EnumGetterByUniqueId(val uniqueId: Int) {

    inline operator fun <reified T> getValue(
        thisRef: Any?,
        property: KProperty<*>
    ): T where T : Enum<T>, T : Unique {
        return enumValues<T>().firstOrNull { it.uniqueId == uniqueId }
            ?: error("You have to enter one of the exist platforms: ${enumValues<Platforms>().joinToString { it.name }}")
    }
}

fun getEnumByUniqueId(uniqueId: Int) = EnumGetterByUniqueId(uniqueId)

// TODO
//fun getEnumByUniqueIdNullable(uniqueId: Int) = EnumGetterByUniqueId(uniqueId)