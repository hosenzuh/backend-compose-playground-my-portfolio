package hussein.com.domain.usecases.images

import hussein.com.data.model.Image
import hussein.com.data.repository.ImagesRepository
import hussein.com.domain.base.InsertDataUseCase
import hussein.com.domain.base.State
import hussein.com.utils.BASE_URL
import hussein.com.utils.saveFilePartToServer
import io.ktor.http.content.*

class InsertImagesForProjectUseCase(private val imagesRepository: ImagesRepository) : InsertDataUseCase<List<Image>>() {

    operator fun invoke(images: List<PartData.FileItem>, projectId: Int, projectName: String) = execute {
        images.forEach {
            it.saveFilePartToServer("static/projectsImages/${projectName}")
        }
        val imagesPaths = images.map { "${BASE_URL}/static/projectsImages/${projectName}/${it.originalFileName}" }

        (imagesRepository.insertImage(imagesPaths, projectId) as State<List<Image>>)
    }
}