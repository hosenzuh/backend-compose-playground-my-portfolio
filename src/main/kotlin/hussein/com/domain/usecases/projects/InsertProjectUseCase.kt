package hussein.com.domain.usecases.projects

import hussein.com.data.repository.ProjectsRepository
import hussein.com.domain.base.InsertDataUseCase
import hussein.com.domain.base.State
import hussein.com.domain.model.ExposedProject
import hussein.com.domain.usecases.images.InsertImagesForProjectUseCase
import hussein.com.utils.BASE_URL
import hussein.com.utils.saveFilePartToServer
import io.ktor.http.content.*
import kotlinx.coroutines.flow.last

class InsertProjectUseCase(
    private val projectsRepository: ProjectsRepository,
    private val insertImagesForProjectUseCase: InsertImagesForProjectUseCase
) :
    InsertDataUseCase<ExposedProject>() {

    operator fun invoke(
        name: String,
        description: String,
        video: PartData.FileItem?,
        link: String?,
        companyId: Int?,
        skills: String,
        images: List<PartData.FileItem>,
        logo: PartData.FileItem?,
    ) = execute {

        video?.saveFilePartToServer("static/projectsVideos/${name}")
        val videoPath = video?.let { "${BASE_URL}/static/projectsVideos/${name}/${video.originalFileName}" }

        logo?.saveFilePartToServer("static/projectsLogos/${name}")
        val logoPath = logo?.let { "${BASE_URL}/static/projectsLogos/${name}/${logo.originalFileName}" }

        val projectState =
            projectsRepository.insertProject(name, description, videoPath, link, companyId, skills, logoPath)
        if (projectState is State.Success.DataInserted) {
            val insertImagesState =
                insertImagesForProjectUseCase(images, projectState.data.id, projectState.data.name).last()

            if (insertImagesState is State.Success.DataInserted) {
                with(projectState.data) {
                    State.Success.DataInserted(
                        ExposedProject(
                            id,
                            name,
                            description,
                            videoPath,
                            link,
                            workExpertiseId,
                            insertImagesState.data,
                            skills.split(","),
                            logoPath
                        )
                    )
                }
            } else
                State.failure()
        } else
            State.failure()
    }
}
