package hussein.com.domain.usecases.projects

import hussein.com.data.model.WorkExpertise
import hussein.com.data.repository.ProjectsRepository
import hussein.com.domain.base.GetDataListUseCase
import hussein.com.domain.model.ExposedProject

class GetProjectsByWorkExpertiseIdUseCase(private val projectsRepository: ProjectsRepository) :
    GetDataListUseCase<ExposedProject>() {

    operator fun invoke(workExpertiseId: Int) = execute {
        projectsRepository.getProjectsByWorkExpertiseId(workExpertiseId)
    }
}