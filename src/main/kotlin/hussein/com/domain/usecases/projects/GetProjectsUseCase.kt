package hussein.com.domain.usecases.projects

import hussein.com.data.repository.ProjectsRepository
import hussein.com.domain.base.GetDataListUseCase
import hussein.com.domain.model.ExposedProject

class GetProjectsUseCase(private val projectsRepository: ProjectsRepository) : GetDataListUseCase<ExposedProject>() {

    operator fun invoke() = execute(projectsRepository::getProjects)
}