package hussein.com.domain.usecases.articles

import hussein.com.data.model.Article
import hussein.com.data.repository.ArticlesRepository
import hussein.com.domain.base.EditDataUseCase
import hussein.com.utils.BASE_ARTICLES_IMAGES_URL
import hussein.com.utils.BASE_URL
import hussein.com.utils.Platforms
import hussein.com.utils.saveFilePartToServer
import io.ktor.http.content.*

class EditArticleUseCase(private val articlesRepository: ArticlesRepository) : EditDataUseCase<Article>() {

    operator fun invoke(
        id: Int,
        title: String,
        content: String,
        author: String,
        publishingDate: String?,
        categories: String,
        url: String,
        reactionsCount: Int?,
        commentsCount: Int?,
        platform: Platforms,
        image: PartData.FileItem?,
    ) = execute {

        val fullPath = "$BASE_URL/$BASE_ARTICLES_IMAGES_URL/${author}/${title}/${image?.originalFileName}"
        val isAdded = image?.saveFilePartToServer("$BASE_ARTICLES_IMAGES_URL/${author}/${title}")

        articlesRepository.editArticle(
            id = id,
            title = title,
            content = content,
            author = author,
            publishingDate = publishingDate,
            categories = categories,
            url = url,
            reactionsCount = reactionsCount,
            commentsCount = commentsCount,
            platform = platform,
            imagePath = if (isAdded == true) fullPath else null,
        )
    }
}