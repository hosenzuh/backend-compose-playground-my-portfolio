package hussein.com.domain.usecases.articles

import hussein.com.data.database.entity.ArticleEntity
import hussein.com.data.model.Article
import hussein.com.data.repository.ArticlesRepository
import hussein.com.domain.base.GetDataListUseCase
import hussein.com.domain.base.State
import kotlinx.coroutines.flow.flow

class GetArticlesUseCase(private val articlesRepository: ArticlesRepository) : GetDataListUseCase<Article>() {

    operator fun invoke() = execute {
        articlesRepository.getArticles()
    }
}