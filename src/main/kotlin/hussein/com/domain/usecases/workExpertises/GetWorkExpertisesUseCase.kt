package hussein.com.domain.usecases.workExpertises

import hussein.com.data.repository.WorkExpertisesRepository
import hussein.com.domain.base.GetDataListUseCase
import hussein.com.domain.model.ExposedWorkExpertise

class GetWorkExpertisesUseCase(private val workExpertisesRepository: WorkExpertisesRepository) :
    GetDataListUseCase<ExposedWorkExpertise>() {

    operator fun invoke() = execute(workExpertisesRepository::getWorkExpertises)

}