package hussein.com.domain.usecases.workExpertises

import hussein.com.data.repository.WorkExpertisesRepository
import hussein.com.domain.base.InsertDataUseCase
import hussein.com.domain.model.ExposedWorkExpertise
import hussein.com.utils.BASE_URL
import hussein.com.utils.saveFilePartToServer
import io.ktor.http.content.*

class InsertWorkExpertiseUseCase(private val workExpertisesRepository: WorkExpertisesRepository) :
    InsertDataUseCase<ExposedWorkExpertise>() {

    operator fun invoke(
        name: String,
        image: PartData.FileItem?,
        description: String,
        startDate: String,
        endDate: String?,
        link: String?
    ) = execute {

        image?.saveFilePartToServer("static/workExpertisesImages/$name")
        val imagePath = image?.let { "$BASE_URL/static/workExpertisesImages/$name/${image.originalFileName}" }

        workExpertisesRepository.insertWorkExpertise(
            name, imagePath, description, startDate, endDate, link
        )
    }

}