package hussein.com.domain.model

import hussein.com.data.model.Image
import kotlinx.serialization.Serializable

@Serializable
data class ExposedProject(
    val id: Int,
    val name: String,
    val description: String,
    val video: String?,
    val link: String?,
    val workExpertiseId: Int?,
    val images: List<Image>,
    val skills: List<String>,
    val logo: String?,
)
