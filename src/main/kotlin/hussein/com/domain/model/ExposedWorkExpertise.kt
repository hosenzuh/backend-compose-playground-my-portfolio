package hussein.com.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class ExposedWorkExpertise(
    val id: Int,
    val companyName: String,
    val companyImage: String?,
    val startDate: String,
    val endDate: String?,
    val description: String,
    val link: String?,
    val projects: List<ExposedProject>,
)
