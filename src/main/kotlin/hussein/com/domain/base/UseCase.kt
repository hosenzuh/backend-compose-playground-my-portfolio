package hussein.com.domain.base

import io.ktor.http.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow


abstract class UseCase

abstract class InsertDataUseCase<T> {

    protected fun execute(request: suspend () -> State<T>): Flow<State<T>> = flow<State<T>> {

        emit(State.loading())

        emit(request())


    }.catch { throwable ->
        println(throwable)
        emit(State.failure(throwable.message, HttpStatusCode.InternalServerError))
    }
}

abstract class EditDataUseCase<T> {

    protected fun execute(request: suspend () -> State<T>): Flow<State<T>> = flow<State<T>> {

        emit(State.loading())

        val resultState = request()
        if (resultState is State.Success.Data && resultState.data == null) {
            emit(State.Failure.ItemNotFound())
        } else {
            emit(resultState)
        }

    }.catch { throwable ->
        println(throwable)
        emit(State.failure(throwable.message, HttpStatusCode.InternalServerError))
    }
}

abstract class GetDataItemUseCase<T> {

    protected fun execute(request: suspend () -> State<T>): Flow<State<T>> = flow {

        emit(State.loading())

        val resultState = request()
        if (resultState is State.Success.Data && resultState.data == null) {
            emit(State.Failure.ItemNotFound())
        } else {
            emit(resultState)
        }

    }.catch { throwable ->
        println(throwable)
        emit(State.failure(throwable.message, HttpStatusCode.InternalServerError))
    }
}

abstract class GetDataListUseCase<T> {

    protected fun execute(request: suspend () -> State<List<T>>): Flow<State<List<T>>> = flow<State<List<T>>> {

        emit(State.loading())

        emit(request())

    }.catch { throwable ->
        println(throwable)
        emit(State.failure(throwable.message, HttpStatusCode.InternalServerError))
    }
}