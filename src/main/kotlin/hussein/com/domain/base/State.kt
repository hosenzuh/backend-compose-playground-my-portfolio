package hussein.com.domain.base

import io.ktor.http.*
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

@OptIn(ExperimentalContracts::class)
sealed class State<T>() {

    abstract val message: String?

    companion object {

        fun <T> initial(message: String? = null) = Initial<T>(message)

        fun <T> loading(message: String? = null) = Loading.Generic<T>(message)

        fun <T> success(data: T?, message: String? = null) =
            if (data != null)
                Success.Data<T>(data, message)
            else
                Success.Generic<T>(message)

        fun <T> failure(message: String? = null, httpStatusCode: HttpStatusCode = HttpStatusCode.BadRequest) =
            Failure.Generic<T>(message, httpStatusCode)

    }

    data class Initial<T>(override val message: String?) : State<T>()

    sealed class Loading<T> : State<T>() {

        data class Generic<T>(override val message: String?) : Loading<T>()

    }

    sealed class Success<T> : State<T>() {

        data class Generic<T>(override val message: String? = null) : Success<T>()

        data class DataInserted<T>(val data: T, override val message: String? = null) : Success<T>()

        data class DataPartiallyInserted<T>(val data: T, override val message: String? = null) : Success<T>()

        data class DataEdited<T>(val data: T, override val message: String? = null) : Success<T>()

        data class Data<T>(val data: T, override val message: String? = null) : Success<T>()

    }

    sealed class Failure<T>(
    ) : State<T>() {
        open val httpStatusCode: HttpStatusCode = HttpStatusCode.BadRequest

        data class Generic<T>(
            override val message: String? = null,
            override val httpStatusCode: HttpStatusCode
        ) : Failure<T>()

        data class ItemNotFound<T>(
            override val message: String? = null,
            override val httpStatusCode: HttpStatusCode = HttpStatusCode.NotFound
        ) : Failure<T>()

    }

    val dataOrNull get() = if (this is Success.Data<T>) data else null

    val isInitial get() = this is Initial

    val isLoading get() = this is Loading

    val isSuccess get() = this is Success

    fun isSuccessWithData(): Boolean {
        contract {
            returns(true) implies (this@State is Success.Data<T>)
        }
        return this@State is Success.Data<T>
    }

    fun isFailure(): Boolean {
        contract {
            returns(true) implies (this@State is Failure<T>)
        }
        return this is Failure<T>
    }

    fun <E> transform(transformation: (T) -> E) = if (this is Success.Data) {
        Success.Data<E>(transformation(data))
    } else {
        this as State<E>
    }
}