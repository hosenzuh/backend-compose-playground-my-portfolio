package hussein.com.domain.base

interface Unique {
    val uniqueId: Int
}