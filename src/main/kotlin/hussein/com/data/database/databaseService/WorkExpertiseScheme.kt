package hussein.com.data.database.databaseService

import hussein.com.data.database.AppDatabase
import hussein.com.data.model.WorkExpertise
import hussein.com.domain.model.ExposedWorkExpertise
import hussein.com.utils.dbQuery
import hussein.com.utils.loggedTransaction
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction


object WorkExpertisesService {

    init {
        loggedTransaction {
            SchemaUtils.create(WorkExpertises)
        }
    }

    suspend fun getWorkExpertises(): List<ExposedWorkExpertise> = dbQuery {
        WorkExpertises.selectAll()
            .map {
                getExposedWorkExpertiseFromResultRow(it)
            }
    }

    suspend fun insertWorkExpertise(
        name: String,
        image: String?,
        description: String,
        startDate: String,
        endDate: String?,
        link: String?,
    ): ExposedWorkExpertise? = dbQuery {
        WorkExpertises.insert {
            it[this.name] = name
            it[this.image] = image
            it[this.description] = description
            it[this.startDate] = startDate
            it[this.endDate] = endDate
            it[this.link] = link
        }.resultedValues?.first()?.let {
            getExposedWorkExpertiseFromResultRow(it)
        }
    }

    suspend fun editWorkExpertise(
        id: Int,
        name: String,
        image: String?,
        description: String,
        startDate: String,
        endDate: String?,
        link: String?,
    ) = dbQuery {
        WorkExpertises.update(where = {
            WorkExpertises.id eq id
        }) {
            it[this.name] = name
            it[this.image] = image
            it[this.description] = description
            it[this.startDate] = startDate
            it[this.endDate] = endDate
            it[this.link] = link
        }.takeIf { it == 1 }?.let {
            WorkExpertises.select { WorkExpertises.id eq id }.first().let { getExposedWorkExpertiseFromResultRow(it) }
        }
    }

    // Helpers

    private suspend fun getExposedWorkExpertiseFromResultRow(resultRow: ResultRow): ExposedWorkExpertise {
        val workExpertise = WorkExpertise.fromResultRow(resultRow)
        val projects = ProjectService.getProjectsByWorkExpertiseId(workExpertise.id)
        return workExpertise.run {
            ExposedWorkExpertise(id, companyName, companyImage, startDate, endDate, description, link, projects)
        }
    }
}

object WorkExpertises : IntIdTable() {
    val name = varchar("name", length = 50)
    val image = varchar("image", length = 2000).nullable()
    val description = text("description")
    val startDate = varchar("start_date", length = 50)
    val endDate = varchar("end_date", length = 50).nullable()
    val link = varchar("link", length = 2000).nullable()
}