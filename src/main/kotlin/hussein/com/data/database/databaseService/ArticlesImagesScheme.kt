package hussein.com.data.database.databaseService

import hussein.com.data.database.entity.ArticleImageEntity
import hussein.com.data.model.Image
import hussein.com.utils.dbQuery
import hussein.com.utils.loggedTransaction
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.*


object ArticlesImagesScheme {

    init {
        loggedTransaction {
            SchemaUtils.create(ArticleImages)
        }
    }

    suspend fun insertImage(path: String): ArticleImageEntity {
        return dbQuery {
            ArticleImageEntity.new {
                this.path = path
            }
        }
    }
}

object ArticleImages : IntIdTable() {
    val path = varchar("path", 2000)
}



