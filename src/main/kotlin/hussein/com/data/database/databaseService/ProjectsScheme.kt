package hussein.com.data.database.databaseService

import hussein.com.data.database.AppDatabase
import hussein.com.data.model.Project
import hussein.com.domain.model.ExposedProject
import hussein.com.utils.dbQuery
import hussein.com.utils.loggedTransaction
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction


object ProjectService {

    init {
        loggedTransaction {
            SchemaUtils.create(Projects)
        }
    }

    suspend fun getProjects(): List<ExposedProject> = dbQuery {
        Projects.selectAll()
            .map {
                getExposedProjectFromResultRow(it)
            }
    }

    suspend fun getProjectsByWorkExpertiseId(workExpertiseId: Int): List<ExposedProject> =
        dbQuery {
            Projects.selectBatched {
                Projects.workExpertiseId eq workExpertiseId
            }.flatten().map {
                getExposedProjectFromResultRow(it)
            }
        }

    suspend fun insertProject(
        name: String,
        description: String,
        video: String?,
        link: String?,
        companyId: Int?,
        skills: String,
        logo: String?,
    ): Project? = dbQuery {
        Projects.insert {
            it[this.name] = name
            it[this.description] = description
            it[this.video] = video
            it[this.link] = link
            it[this.workExpertiseId] = companyId
            it[this.skills] = skills
            it[this.logo] = logo
        }.resultedValues?.first()?.let {
            Project.fromRowResult(it)
        }
    }

    suspend fun editProject(
        id: Int,
        name: String,
        description: String,
        video: String?,
        link: String?,
        companyId: Int?,
        skills: String,
        logo: String?,
    ): ExposedProject? = dbQuery {
        Projects.update(where = { Projects.id eq id }) {
            it[this.name] = name
            it[this.description] = description
            it[this.video] = video
            it[this.link] = link
            it[this.workExpertiseId] = companyId
            it[this.skills] = skills
            it[this.logo] = logo
        }.takeIf { it == 1 }?.let {
            Projects.select { Projects.id eq id }.first().run {
                getExposedProjectFromResultRow(this)
            }
        }
    }

    // Helpers

    private suspend fun getExposedProjectFromResultRow(it: ResultRow): ExposedProject {
        val project = Project.fromRowResult(it)
        val images = ImagesService.getImagesByProjectId(project.id)

        return ExposedProject(
            project.id,
            project.name,
            project.description,
            project.video,
            project.link,
            project.workExpertiseId,
            images,
            project.skills.split(","),
            project.logo
        )
    }

}

object Projects : IntIdTable() {
    val name = varchar("name", length = 50)
    val description = text("description")
    val video = varchar("video", length = 2000).nullable()
    val link = varchar("link", length = 2000).nullable()
    val workExpertiseId = reference("work_expertise_id", WorkExpertises.id).nullable()
    val skills = text("skills")
    val logo = text("logo").nullable()
}