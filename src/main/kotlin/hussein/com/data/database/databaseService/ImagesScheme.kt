package hussein.com.data.database.databaseService

import hussein.com.data.model.Image
import hussein.com.utils.dbQuery
import hussein.com.utils.loggedTransaction
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction


object ImagesService {

    init {
        loggedTransaction {
            SchemaUtils.create(Images)
        }
    }

    suspend fun insertImages(imagesPaths: List<String>, imageProjectId: Int): List<Image> {
        return dbQuery {
            Images.batchInsert(imagesPaths) {
                this[Images.path] = it
                this[Images.projectId] = imageProjectId
            }.map {
                Image(it[Images.id].value, it[Images.path], it[Images.projectId].value)
            }
        }
    }

    suspend fun getImagesByProjectId(projectId: Int): List<Image> {
        return dbQuery {
            val images = Images.select {
                Images.projectId eq projectId
            }.map {
                Image(it[Images.id].value, it[Images.path], projectId)
            }

            images
        }
    }
}

object Images : IntIdTable() {
    val path = varchar("path", 2000)
    val projectId = reference("projectId", Projects.id)
}