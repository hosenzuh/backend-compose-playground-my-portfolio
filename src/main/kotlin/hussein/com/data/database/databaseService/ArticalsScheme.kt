package hussein.com.data.database.databaseService

import hussein.com.data.database.entity.ArticleEntity
import hussein.com.data.database.entity.ArticleImageEntity
import hussein.com.data.model.Article
import hussein.com.utils.Platforms
import hussein.com.utils.dbQuery
import hussein.com.utils.loggedTransaction
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.dao.load
import org.jetbrains.exposed.dao.with
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

object Articles : IntIdTable() {
    val title = text("title")
    val content = text("content")
    val author = text("author")
    val publishingDate = text("publishingDate").nullable()
    val categories = text("categories")
    val url = text("url")
    val reactionsCount = integer("reactionCount").nullable()
    val commentsCount = integer("commentsCount").nullable()
    val platform = enumeration<Platforms>("platform")
    val image = reference("image", ArticleImages, onDelete = ReferenceOption.SET_NULL).nullable()
}

object ArticlesService {

    init {
        loggedTransaction {
            SchemaUtils.create(Articles)
        }
    }

    suspend fun getArticles() =
        dbQuery { ArticleEntity.all().with(ArticleEntity::image).toList() }

    suspend fun insertArticle(
        title: String,
        content: String,
        author: String,
        publicationDate: String?,
        categories: String,
        url: String,
        reactionsCount: Int?,
        commentsCount: Int?,
        platform: Platforms,
        imagePath: String?,
    ) = dbQuery {
        val imageEntity = imagePath?.let { ArticlesImagesScheme.insertImage(it) }
        ArticleEntity.new {
            this.title = title
            this.content = content
            this.author = author
            this.publishingDate = publicationDate
            this.categories = categories
            this.url = url
            this.reactionsCount = reactionsCount
            this.commentsCount = commentsCount
            this.platform = platform
            this.image = imageEntity
        }.load(ArticleEntity::image)

    }

    suspend fun editArticle(
        id: Int,
        title: String,
        content: String,
        author: String,
        publicationDate: String?,
        categories: String,
        url: String,
        reactionsCount: Int?,
        commentsCount: Int?,
        platform: Platforms,
        imagePath: String?,
    ): ArticleEntity? = dbQuery {
        val imageEntity = imagePath?.let { ArticlesImagesScheme.insertImage(it) }

        ArticleEntity.findById(id)?.apply {
            this.title = title
            this.content = content
            this.author = author
            this.publishingDate = publicationDate
            this.categories = categories
            this.url = url
            this.reactionsCount = reactionsCount
            this.commentsCount = commentsCount
            this.platform = platform

            this.image = imageEntity
        }?.load(ArticleEntity::image)
    }


    // helpers

    private fun getArticleFromRowResult(rowRow: ResultRow) =
        with(rowRow) {
            Article(
                get(Articles.id).value,
                get(Articles.title),
                get(Articles.content),
                get(Articles.author),
                get(Articles.publishingDate),
                get(Articles.categories).split(","),
                get(Articles.url),
                get(Articles.reactionsCount),
                get(Articles.commentsCount),
                get(Articles.platform),
                null
            )
        }

}