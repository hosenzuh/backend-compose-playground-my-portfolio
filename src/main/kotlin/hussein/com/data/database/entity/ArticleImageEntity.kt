package hussein.com.data.database.entity

import hussein.com.data.database.databaseService.ArticleImages
import hussein.com.data.model.ArticleImage
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class ArticleImageEntity(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<ArticleImageEntity>(ArticleImages)

    var path by ArticleImages.path

    fun toImage() = ArticleImage(id.value, path)
}