package hussein.com.data.database.entity

import hussein.com.data.database.databaseService.ArticleImages
import hussein.com.data.database.databaseService.Articles
import hussein.com.data.model.Article
import hussein.com.utils.Platforms
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID


class ArticleEntity(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<ArticleEntity>(Articles)

    var title by Articles.title
    var content by Articles.content
    var author by Articles.author
    var publishingDate by Articles.publishingDate
    var categories by Articles.categories
    var url by Articles.url
    var reactionsCount by Articles.reactionsCount
    var commentsCount by Articles.commentsCount
    var platform: Platforms by Articles.platform
    var image by ArticleImageEntity optionalReferencedOn Articles.image

    fun toArticle() = Article(
        id.value,
        title = title,
        content = content,
        author = author,
        publishingDate = publishingDate,
        categories = categories.split(","),
        url = url,
        reactionsCount = reactionsCount,
        commentsCount = commentsCount,
        platform = platform,
        image = image?.toImage()
    )
}