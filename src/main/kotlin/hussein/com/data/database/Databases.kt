package hussein.com.data.database

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.DatabaseConfig
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.transaction

object AppDatabase {

    val database: Database = Database.connect(
        url = "jdbc:h2:file:./database/db;DB_CLOSE_DELAY=-1",
        user = "root",
        driver = "org.h2.Driver",
        password = "",
        databaseConfig = DatabaseConfig {
            keepLoadedReferencesOutOfTransaction = true
        }
    )

}
