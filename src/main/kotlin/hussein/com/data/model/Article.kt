package hussein.com.data.model

import hussein.com.utils.Platforms
import kotlinx.serialization.Serializable


@Serializable
data class Article(
    val id: Int,
    val title: String,
    val content: String,
    val author: String,
    val publishingDate: String?,
    val categories: List<String>,
    val url: String,
    val reactionsCount: Int?,
    val commentsCount: Int?,
    val platform: Platforms,
    val image: ArticleImage?,
)
