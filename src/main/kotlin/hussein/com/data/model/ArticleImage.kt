package hussein.com.data.model

import kotlinx.serialization.Serializable

@Serializable
data class ArticleImage(
    val id: Int,
    val path: String
)
