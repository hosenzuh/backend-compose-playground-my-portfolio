package hussein.com.data.model

import hussein.com.data.database.databaseService.ProjectService
import hussein.com.data.database.databaseService.Projects
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.ResultRow

@Serializable
data class Project(
    val id: Int,
    val name: String,
    val description: String,
    val video: String?,
    val link: String?,
    val workExpertiseId: Int?,
    val skills: String,
    val logo: String?,
) {
    companion object {
        fun fromRowResult(resultRow: ResultRow): Project {
            return Project(
                resultRow[Projects.id].value,
                resultRow[Projects.name],
                resultRow[Projects.description],
                resultRow[Projects.video],
                resultRow[Projects.link],
                resultRow[Projects.workExpertiseId]?.value,
                resultRow[Projects.skills],
                resultRow[Projects.logo],
            )
        }
    }
}
