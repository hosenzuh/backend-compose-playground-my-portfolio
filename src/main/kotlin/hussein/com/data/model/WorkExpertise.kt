package hussein.com.data.model

import hussein.com.data.database.databaseService.WorkExpertises
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.ResultRow

@Serializable
data class WorkExpertise(
    val id: Int,
    val companyName: String,
    val companyImage: String?,
    val description: String,
    val startDate: String,
    val endDate: String?,
    val link: String?,
) {
    companion object {
        fun fromResultRow(resultRow: ResultRow) =
            WorkExpertise(
                resultRow[WorkExpertises.id].value,
                resultRow[WorkExpertises.name],
                resultRow[WorkExpertises.image],
                resultRow[WorkExpertises.description],
                resultRow[WorkExpertises.startDate],
                resultRow[WorkExpertises.endDate],
                resultRow[WorkExpertises.link],
            )
    }
}