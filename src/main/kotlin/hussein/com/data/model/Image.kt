package hussein.com.data.model

import kotlinx.serialization.Serializable

@Serializable
data class Image(val id: Int, val path: String, val projectId: Int)
