package hussein.com.data.repository.base

import hussein.com.domain.base.State
import io.ktor.http.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

abstract class BaseRepository {

    protected suspend fun <T> getData(request: suspend () -> T?): State<T> {
        val result = request()
        return withContext(Dispatchers.IO) {
            if (result != null) {
                State.success(result, message = null)
            } else {
                State.failure(message = null, HttpStatusCode.NotFound)
            }
        }
    }

    protected suspend fun <T> insertRow(request: suspend () -> T?): State<T> {
        val result = request()
        return withContext(Dispatchers.IO) {
            if (result != null) {
                State.Success.DataInserted(result, message = null)
            } else {
                State.failure(message = null, HttpStatusCode.NotFound)
            }
        }
    }

    protected suspend fun <T> editRow(request: suspend () -> T?): State<T> {
        val result = request()
        return withContext(Dispatchers.IO) {
            if (result != null) {
                State.Success.DataEdited(result, message = null)
            } else {
                State.Failure.ItemNotFound(message = null, HttpStatusCode.NotFound)
            }
        }
    }

    protected suspend fun <T> insertRows(itemsCount: Int, request: suspend () -> List<T>): State<List<T>> {
        val result = request()
        return withContext(Dispatchers.IO) {
            when (result.size) {
                itemsCount -> {
                    State.Success.DataInserted(result, message = null)
                }

                in 1 ..< itemsCount -> {
                    State.Success.DataPartiallyInserted(result, message = null)
                }

                else -> {
                    State.Failure.ItemNotFound(message = null, HttpStatusCode.NotFound)
                }
            }
        }
    }

}