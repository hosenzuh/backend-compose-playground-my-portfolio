package hussein.com.data.repository

import hussein.com.data.database.databaseService.ArticlesService
import hussein.com.data.database.entity.ArticleEntity
import hussein.com.data.repository.base.BaseRepository
import hussein.com.utils.Platforms
import hussein.com.utils.dbQuery


class ArticlesRepository : BaseRepository() {

    suspend fun getArticles() = getData {
        ArticlesService.getArticles().map(ArticleEntity::toArticle)
    }

    suspend fun insertArticle(
        title: String,
        content: String,
        author: String,
        publishingDate: String?,
        categories: String,
        url: String,
        reactionsCount: Int?,
        commentsCount: Int?,
        platform: Platforms,
        imagePath: String?,
    ) = insertRow {
        ArticlesService.insertArticle(
            title = title,
            content = content,
            author = author,
            publicationDate = publishingDate,
            categories = categories,
            url = url,
            reactionsCount = reactionsCount,
            commentsCount = commentsCount,
            platform = platform,
            imagePath,
        ).toArticle()
    }

    suspend fun editArticle(
        id: Int,
        title: String,
        content: String,
        author: String,
        publishingDate: String?,
        categories: String,
        url: String,
        reactionsCount: Int?,
        commentsCount: Int?,
        platform: Platforms,
        imagePath: String?,
    ) = editRow {
        ArticlesService.editArticle(
            id = id,
            title = title,
            content = content,
            author = author,
            publicationDate = publishingDate,
            categories = categories,
            url = url,
            reactionsCount = reactionsCount,
            commentsCount = commentsCount,
            platform = platform,
            imagePath = imagePath
        )?.toArticle()
    }
}