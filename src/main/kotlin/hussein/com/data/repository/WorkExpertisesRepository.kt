package hussein.com.data.repository

import hussein.com.data.database.databaseService.WorkExpertisesService
import hussein.com.data.repository.base.BaseRepository

class WorkExpertisesRepository : BaseRepository() {

    suspend fun getWorkExpertises() = getData {
        WorkExpertisesService.getWorkExpertises()
    }

    suspend fun insertWorkExpertise(
        name: String,
        image: String?,
        description: String,
        startDate: String,
        endDate: String?,
        link: String?
    ) = insertRow {
        WorkExpertisesService.insertWorkExpertise(
            name,
            image,
            description,
            startDate,
            endDate,
            link,
        )
    }

    suspend fun editWorkExpertise(
        id:Int,
        name: String,
        image: String?,
        description: String,
        startDate: String,
        endDate: String?,
        link: String?
    ) = editRow {
        WorkExpertisesService.editWorkExpertise(
            id, name, image, description, startDate, endDate, link
        )
    }

}