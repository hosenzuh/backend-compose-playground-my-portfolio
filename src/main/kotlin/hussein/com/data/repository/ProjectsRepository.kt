package hussein.com.data.repository

import hussein.com.data.database.databaseService.ProjectService
import hussein.com.data.model.Project
import hussein.com.data.repository.base.BaseRepository
import hussein.com.domain.base.State
import hussein.com.domain.model.ExposedProject

class ProjectsRepository : BaseRepository() {


    suspend fun getProjects(): State<List<ExposedProject>> =
        getData {
            ProjectService.getProjects()
        }

    suspend fun insertProject(
        name: String,
        description: String,
        video: String?,
        link: String?,
        companyId: Int?,
        skills: String,
        logo: String?,
    ): State<Project> = insertRow {
        ProjectService.insertProject(name, description, video, link, companyId, skills, logo)
    }

    suspend fun editProject(
        id: Int,
        name: String,
        description: String,
        video: String?,
        link: String?,
        companyId: Int?,
        skills: String,
        logo: String?,
    ): State<ExposedProject> = editRow {
        ProjectService.editProject(id, name, description, video, link, companyId, skills, logo)
    }

    suspend fun getProjectsByWorkExpertiseId(workExpertiseId: Int) = getData {
        ProjectService.getProjectsByWorkExpertiseId(workExpertiseId)
    }

}