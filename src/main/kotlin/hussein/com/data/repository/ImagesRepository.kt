package hussein.com.data.repository

import hussein.com.data.database.databaseService.ImagesService
import hussein.com.data.repository.base.BaseRepository


class ImagesRepository : BaseRepository() {

    suspend fun insertImage(imagePaths: List<String>, projectId: Int) = insertRows(itemsCount = imagePaths.size) {
        ImagesService.insertImages(imagePaths, projectId)
    }
}