package hussein.com.di

import hussein.com.data.repository.ArticlesRepository
import hussein.com.data.repository.ImagesRepository
import hussein.com.data.repository.ProjectsRepository
import hussein.com.data.repository.WorkExpertisesRepository
import hussein.com.domain.usecases.articles.EditArticleUseCase
import hussein.com.domain.usecases.articles.GetArticlesUseCase
import hussein.com.domain.usecases.articles.InsertArticleUseCase
import hussein.com.domain.usecases.images.InsertImagesForProjectUseCase
import hussein.com.domain.usecases.projects.EditProjectUseCase
import hussein.com.domain.usecases.projects.GetProjectsByWorkExpertiseIdUseCase
import hussein.com.domain.usecases.projects.GetProjectsUseCase
import hussein.com.domain.usecases.projects.InsertProjectUseCase
import hussein.com.domain.usecases.workExpertises.EditWorkExpertiseUseCase
import hussein.com.domain.usecases.workExpertises.GetWorkExpertisesUseCase
import hussein.com.domain.usecases.workExpertises.InsertWorkExpertiseUseCase
import org.koin.core.module.dsl.factoryOf
import org.koin.core.module.dsl.scopedOf
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module
import org.koin.ktor.plugin.RequestScope
import java.util.UUID


val appModule = module {
    scope<RequestScope> {
        scopedOf(::ScopeComponent)
    }

    singleOf(::WorkExpertisesRepository)
    singleOf(::ProjectsRepository)
    singleOf(::ImagesRepository)
    singleOf(::ArticlesRepository)

    // Use-cases
    factoryOf(::InsertProjectUseCase)
    factoryOf(::EditProjectUseCase)
    factoryOf(::GetProjectsUseCase)
    factoryOf(::GetProjectsByWorkExpertiseIdUseCase)

    factoryOf(::InsertWorkExpertiseUseCase)
    factoryOf(::GetWorkExpertisesUseCase)
    factoryOf(::EditWorkExpertiseUseCase)

    factoryOf(::InsertImagesForProjectUseCase)

    factoryOf(::InsertArticleUseCase)
    factoryOf(::GetArticlesUseCase)
    factoryOf(::EditArticleUseCase)

}

class ScopeComponent {
    val id = UUID.randomUUID().toString()
}