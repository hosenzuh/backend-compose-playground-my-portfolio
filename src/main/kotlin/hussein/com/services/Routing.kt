package hussein.com.services

import hussein.com.di.ScopeComponent
import hussein.com.domain.base.State
import hussein.com.domain.usecases.articles.EditArticleUseCase
import hussein.com.domain.usecases.articles.GetArticlesUseCase
import hussein.com.domain.usecases.articles.InsertArticleUseCase
import hussein.com.domain.usecases.projects.EditProjectUseCase
import hussein.com.domain.usecases.projects.GetProjectsUseCase
import hussein.com.domain.usecases.projects.InsertProjectUseCase
import hussein.com.domain.usecases.workExpertises.EditWorkExpertiseUseCase
import hussein.com.domain.usecases.workExpertises.GetWorkExpertisesUseCase
import hussein.com.domain.usecases.workExpertises.InsertWorkExpertiseUseCase
import hussein.com.utils.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.server.application.*
import io.ktor.server.http.content.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.coroutines.flow.last
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.internal.decodeStringToJsonTree
import org.koin.ktor.plugin.scope
import java.io.File


fun Application.routing() {

    routing {

        baseRouting()

        projectsRouting()

        workExpertisesRouting()

        articlesRouting()
    }
}

private fun Routing.baseRouting() {
    staticFiles("/static", File("static"))

    head("/ping") {
        call.respond("Pinged successfully")
    }
}

private fun Routing.projectsRouting() {
    get("/projects") {
        val component = call.scope.get<ScopeComponent>()
        val getProjectUseCase by inject<GetProjectsUseCase>()

        val result = getProjectUseCase().last()
        if (result is State.Success.Data) {
            call.respond(HttpStatusCode.OK, result.data)
        } else if (result is State.Failure.Generic) {
            call.respond(result.httpStatusCode, result.message ?: "An error has occurred")
        }
    }

    post("/projects") {
        val component = call.scope.get<ScopeComponent>()
        val insertProjectUseCase by inject<InsertProjectUseCase>()

        val parts: List<PartData> = call.receiveMultipart().readAllParts()

        val name = parts.getFormItemValueByName("name")
        val description = parts.getFormItemValueByName("description")
        val link = parts.getFormItemValueByNameOrNull("link")
        val workExpertiseId = parts.getFormItemValueByNameOrNull("workExpertiseId")?.toIntOrNull()
        val skills = parts.getFormItemValueByName("skills")
        val video = parts.getFileItemByNameOrNull("video")
        val images =
            parts.getFileItemsByPredicate {
                it.contentType?.contentType == ContentType.Image.Any.contentType
                        && it.name != "logo"
            }
        val logo = parts.getFileItemByNameOrNull("logo")

        val insertProjectState =
            insertProjectUseCase(name, description, video, link, workExpertiseId, skills, images, logo)
                .last()
        if (insertProjectState is State.Success.DataInserted) {
            call.respond(
                HttpStatusCode.OK,
                insertProjectState.data
            )
        } else if (insertProjectState is State.Failure) {
            call.respond(insertProjectState.httpStatusCode)
        }

    }

    put("/projects/{id}") {
        val component = call.scope.get<ScopeComponent>()
        val editProjectUseCase by inject<EditProjectUseCase>()

        val parts: List<PartData> = call.receiveMultipart().readAllParts()

        val id = call.parameters["id"]?.toIntOrNull() ?: run {
            call.respond(
                HttpStatusCode.NotAcceptable,
                "You have to pass valid id to the path"
            )
            return@put
        }
        val name = parts.getFormItemValueByName("name")
        val description = parts.getFormItemValueByName("description")
        val link = parts.getFormItemValueByNameOrNull("link")
        val workExpertiseId = parts.getFormItemValueByNameOrNull("workExpertiseId")?.toIntOrNull()
        val skills = parts.getFormItemValueByName("skills")
        val video = parts.getFileItemByNameOrNull("video")
        val images =
            parts.getFileItemsByPredicate {
                it.contentType?.contentType == ContentType.Image.Any.contentType
                        && it.name != "logo"
            }
        val logo = parts.getFileItemByNameOrNull("logo")

        val editProjectState =
            editProjectUseCase(id, name, description, video, link, workExpertiseId, skills, images, logo)
                .last()
        if (editProjectState is State.Success.DataEdited) {
            call.respond(
                HttpStatusCode.OK,
                editProjectState.data
            )
        } else if (editProjectState is State.Failure) {
            call.respond(editProjectState.httpStatusCode)
        }

    }
}

private fun Routing.workExpertisesRouting() {
    get("/workExpertises") {
        call.scope.get<ScopeComponent>()
        val getWorkExpertisesUseCase by inject<GetWorkExpertisesUseCase>()

        val getWorkExpertisesState = getWorkExpertisesUseCase().last()

        if (getWorkExpertisesState is State.Success.Data) {
            call.respond(HttpStatusCode.OK, getWorkExpertisesState.data)
        } else if (getWorkExpertisesState is State.Failure) {
            call.respond(getWorkExpertisesState.httpStatusCode)
        }
    }

    post("/workExpertises") {
        call.scope.get<ScopeComponent>()

        val insertWorkExpertiseUseCase by inject<InsertWorkExpertiseUseCase>()

        val parts: List<PartData> = call.receiveMultipart().readAllParts()

        val name = parts.getFormItemValueByName("name")
        val description = parts.getFormItemValueByName("description")
        val startDate = parts.getFormItemValueByName("startDate")
        val endDate = parts.getFormItemValueByNameOrNull("endDate")
        val link = parts.getFormItemValueByNameOrNull("link")
        val image = parts.getFileItemByNameOrNull("image")

        val insertWorkExpertiseState =
            insertWorkExpertiseUseCase(name, image, description, startDate, endDate, link).last()

        if (insertWorkExpertiseState is State.Success.DataInserted) {
            call.respond(HttpStatusCode.OK, insertWorkExpertiseState.data)
        } else if (insertWorkExpertiseState is State.Failure) {
            call.respond(insertWorkExpertiseState.httpStatusCode)
        }
    }

    put("/workExpertises/{id}") {
        call.scope.get<ScopeComponent>()

        val editWorkExpertiseUseCase by inject<EditWorkExpertiseUseCase>()

        val parts: List<PartData> = call.receiveMultipart().readAllParts()

        val id = call.parameters["id"]?.toIntOrNull() ?: run {
            call.respond(
                HttpStatusCode.NotAcceptable,
                "You have to pass valid id to the path"
            )
            return@put
        }
        val name = parts.getFormItemValueByName("name")
        val description = parts.getFormItemValueByName("description")
        val startDate = parts.getFormItemValueByName("startDate")
        val endDate = parts.getFormItemValueByNameOrNull("endDate")
        val link = parts.getFormItemValueByNameOrNull("link")
        val image = parts.getFileItemByNameOrNull("image")

        val editWorkExpertiseState =
            editWorkExpertiseUseCase(id, name, image, description, startDate, endDate, link).last()

        if (editWorkExpertiseState is State.Success.DataEdited) {
            call.respond(HttpStatusCode.OK, editWorkExpertiseState.data)
        } else if (editWorkExpertiseState is State.Failure) {
            call.respond(editWorkExpertiseState.httpStatusCode)
        }
    }
}

private fun Routing.articlesRouting() {

    get("/articles") {
        call.scope.get<ScopeComponent>()

        val getArticlesUseCase by inject<GetArticlesUseCase>()

        val resultState = getArticlesUseCase().last()
        if (resultState is State.Success.Data) {
            call.respond(HttpStatusCode.OK, resultState.data)
        } else if (resultState is State.Failure) {
            call.respond(resultState.httpStatusCode)
        }

    }

    post("/articles") {
        call.scope.get<ScopeComponent>()

        val insertArticleUseCase by inject<InsertArticleUseCase>()

        val parts = call.receiveMultipart().readAllParts()

        val title = parts.getFormItemValueByName("title")
        val content = parts.getFormItemValueByName("content")
        val author = parts.getFormItemValueByName("author")
        val publishingDate = parts.getFormItemValueByNameOrNull("publishingDate")
        val categories = parts.getFormItemValueByName("categories")
        val url = parts.getFormItemValueByName("url")
        val reactionsCount = parts.getFormItemValueByNameOrNull("reactionsCount")?.toIntOrNull()
        val commentsCount = parts.getFormItemValueByNameOrNull("commentsCount")?.toIntOrNull()
        val platform: Platforms by getEnumByUniqueId(parts.getFormItemValueByName("platform").hashCode())
        val image = parts.getFileItemByNameOrNull("image")

        val articleInsertionState = insertArticleUseCase(
            title = title,
            content = content,
            author = author,
            publishingDate = publishingDate,
            categories = categories,
            url = url,
            reactionsCount = reactionsCount,
            commentsCount = commentsCount,
            platform = platform,
            image = image
        ).last()

        if (articleInsertionState is State.Success.DataInserted) {
            call.respond(HttpStatusCode.OK, articleInsertionState.data)
        } else if (articleInsertionState is State.Failure) {
            call.respond(articleInsertionState.httpStatusCode)
        }
    }

    put("/articles/{id}") {
        call.scope.get<ScopeComponent>()

        val editArticleUseCase by inject<EditArticleUseCase>()

        val parts = call.receiveMultipart().readAllParts()

        val id = call.parameters["id"]?.toIntOrNull() ?: run {
            call.respond(
                HttpStatusCode.NotAcceptable,
                "You have to pass valid id to the path"
            )
            return@put
        }

        val title = parts.getFormItemValueByName("title")
        val content = parts.getFormItemValueByName("content")
        val author = parts.getFormItemValueByName("author")
        val publishingDate = parts.getFormItemValueByNameOrNull("publicationDate")
        val categories = parts.getFormItemValueByName("categories")
        val url = parts.getFormItemValueByName("url")
        val reactionsCount = parts.getFormItemValueByNameOrNull("reactionsCount")?.toIntOrNull()
        val commentsCount = parts.getFormItemValueByNameOrNull("commentsCount")?.toIntOrNull()
        val platform: Platforms by getEnumByUniqueId(parts.getFormItemValueByName("platform").hashCode())
        val image = parts.getFileItemByNameOrNull("image")

        val articleEditingState = editArticleUseCase(
            id,
            title = title,
            content = content,
            author = author,
            publishingDate = publishingDate,
            categories = categories,
            url = url,
            reactionsCount = reactionsCount,
            commentsCount = commentsCount,
            platform = platform,
            image = image
        ).last()

        if (articleEditingState is State.Success.DataEdited) {
            call.respond(HttpStatusCode.OK, articleEditingState.data)
        } else if (articleEditingState is State.Failure) {
            call.respond(articleEditingState.httpStatusCode)
        }
    }

    get("/linkedInPosts") {
        val linkedInPostsJson = File(LINKEDIN_POSTS_PATH).readLines().joinToString("\n")
        val message = Json.decodeFromString<JsonElement>(linkedInPostsJson)
        call.respond(message)
    }

    post("/linkedInPosts") {
        val postsFile = call.receiveMultipart().readPart() ?: error("Some files are missed")
        (postsFile as PartData.FileItem).saveFilePartToServer("static/linkedinPosts/")
        call.respond("Updated Successfully!")
    }
}
