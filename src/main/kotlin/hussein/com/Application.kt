package hussein.com

import hussein.com.data.database.AppDatabase
import hussein.com.data.repository.ImagesRepository
import hussein.com.data.repository.ProjectsRepository
import hussein.com.di.appModule
import hussein.com.plugins.*
import hussein.com.services.routing
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.ktor.plugin.Koin
import org.koin.logger.slf4jLogger

fun main() {
    embeddedServer(Netty, port = 8088, host = "0.0.0.0", module = Application::module)
        .start(wait = true)
}

fun Application.module() {
    install(Koin) {
        slf4jLogger(Level.DEBUG)
        modules(appModule)
    }
    AppDatabase
    configureSerialization()
    routing()
    configureMonitoring()
    configureHTTP()
    configureRouting()

}
